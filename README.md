# Files support for Universe CMS

This package provide `UniCMS.Component`s like `file`, `files` etc.

> This package is part of Universe, a framework based on [Meteor platform](http://meteor.com)
maintained by [Vazco](http://www.vazco.eu).
