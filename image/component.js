'use strict';

var component = new UniCMS.Component('unicms_image', {
    template: 'UniCMSComponentImageOutput',
    schema: {
        type: String
    },
    addInputType: {
        template: 'UniCMSComponentImageInput',
        valueOut: function () {
            return this.val();
        }
    }
});

UniCMS.registerComponent('image', component);
