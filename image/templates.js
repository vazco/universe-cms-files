'use strict';

var setDataOnCreated = function () {
    var component = UniCMS.findComponent();

    if (!component) {
        return;
    }

    check(component, UniCMS.Component);
    this.component = component;

    this.fsCollection = component.fsCollection;
    check(this.fsCollection, FS.Collection);

    this.fileId = new ReactiveVar();

};

var fileObjHelper = function () {
    var templateInstance = Template.instance();
    if (!templateInstance.fsCollection) {
        return;
    }
    return templateInstance.fsCollection.findOne(templateInstance.fileId.get());
};

/*
 Input
 */

Template.UniCMSComponentImageInput.onCreated(setDataOnCreated);
Template.UniCMSComponentImageInput.onCreated(function () {
    var self = this;
    var fsSubName = UniUtils.get(self, 'component.fsSubName');
    var fileId = self.fileId;

    fileId.set(self.data.value); //set initial value

    if (fsSubName) {
        self.autorun(function () {
            self.subscribe(fsSubName, fileId.get());
        });
    }

});

Template.UniCMSComponentImageInput.helpers({
    fileObj: fileObjHelper,
    fileId: function () {
        return Template.instance().fileId.get();
    },
    progress: function () {
        return this.uploadProgress();
    },
    getStore: function() {
        return UniUtils.get(UniUtils.getParentTemplateInstance('uniComponent'), 'data.store');
    }
});

var changeFile = function (file, templateInstance) {
    // @todo set doc._id on file doc when we have it;

    // initiate the upload

    var fsCollection = templateInstance.fsCollection;

    check(fsCollection, FS.Collection);

    fsCollection.insert(file, function (err, fileObj) {
        // Inserted new doc with ID fileObj._id, and kicked off the data upload using HTTP
        if (err) {
            console.log('[UniCMS] Error during file upload. ' + err);
            return;
        }
        templateInstance.fileId.set(fileObj._id);
    });
};

Template.UniCMSComponentImageInput.events({
    'click .js-replace-on-click': function (event, templateInstance) {
        templateInstance.$('input[type="file"]').click();
    },
    'change input[type="file"]': function (event, templateInstance) {

        if (!event.target.files[0]) {
            // no file, e.g. someone clicked cancel
            return;
        }

        var file = new FS.File(event.target.files[0]);
        changeFile(file, templateInstance);

    },
    'dragenter .js-replace-on-click': function (e, tmpl) {
        $('.js-overlay').addClass('hidden');
        tmpl.$('.vfu-label').addClass('hidden');
        tmpl.$('.js-overlay').removeClass('hidden');
        tmpl.$('.js-replace-on-click').addClass('unicms__dropzone__dragover');
    },
    'dragleave .js-replace-on-drop': function (e, tmpl) {
        tmpl.$('.js-overlay').addClass('hidden');
        tmpl.$('.vfu-label').removeClass('hidden');
        tmpl.$('.js-replace-on-click').removeClass('unicms__dropzone__dragover');
    },
    'drop .js-replace-on-drop': function (e, tmpl) {
        tmpl.$('.js-overlay').addClass('hidden');
        tmpl.$('.js-replace-on-click').removeClass('unicms__dropzone__dragover');
    },
    'dropped .js-replace-on-drop': function (e, templateInstance) {
        e = e.originalEvent || e;
        var fileTransfer = UniUtils.get(e, 'dataTransfer.files.0');
        if (!fileTransfer) {
            return;
        }

        var file = new FS.File(fileTransfer);
        changeFile(file, templateInstance);
    }
});

/*
 Output
 */

Template.UniCMSComponentImageOutput.onCreated(setDataOnCreated);
Template.UniCMSComponentImageOutput.onCreated(function () {
    var self = this;
    var fsSubName = UniUtils.get(self, 'component.fsSubName');
    var fileId = self.fileId;

    if (fsSubName) {
        self.autorun(function () {
            var data = Template.currentData();
            fileId.set(data);
            self.subscribe(fsSubName, data || null);
        });
    }

});

Template.UniCMSComponentImageOutput.helpers({
    fileObj: fileObjHelper,
    getLoadingTemplate: function () {
        return UniUtils.get(UniUtils.getParentTemplateInstance('uniComponent'), 'data.loadingTemplate') || null;
    },
    getStore: function() {
        return UniUtils.get(UniUtils.getParentTemplateInstance('uniComponent'), 'data.store');
    }
});
