'use strict';

Package.describe({
    name: 'vazco:universe-cms-files',
    version: '0.0.4',
    summary: 'Files support for UniCMS',
    documentation: 'README.md'
});

Package.onUse(function (api) {
    api.versionsFrom('1.0.4.2');

    api.use([
        'templating'
    ], 'client');

    api.use([
        'underscore'
    ]);

    api.use('vazco:universe-cms');

    // Single image

    api.addFiles([
        'image/templates.html',
        'image/templates.js',
        'image/style.css'
    ], 'client');
    api.addFiles('image/component.js');

});
